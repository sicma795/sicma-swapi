const { Router } = require('express');
const fetch = require('node-fetch');

const { filterFilm, filterStarship } = require('../helpers/filters');

const router = new Router();

router.get('/', (req, res) => {
  fetch('https://swapi.co/api/films/?format=json')
  .then(data => data.json())
  .then(data => data.results.map(film => filterFilm(film)))
  .then(films => res.status(200).send({ status: 1, films }))
  .catch(err => {
    console.log(err);
    res.status(500).send({ status: -1 });
  });
});

router.get('/:id', (req, res) => {
  const id = req.params.id;

  fetch(`https://swapi.co/api/films/${id}/?format=json`)
  .then(data => data.json())
  .then(film => filterFilm(film))
  .then(film => {
    if(film) {
      res.status(200).send({ status: 1, film });
    } else {
      res.status(200).send({ status: -2 });
    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).send({ status: -1 });
  });
});

router.get('/:id/starships', (req, res) => {
  const id = req.params.id;

  fetch(`https://swapi.co/api/films/${id}/?format=json`)
  .then(data => data.json())
  .then(film => film.starships)
  .then(urls => urls.map(url => fetch(url)))
  .then(promises => Promise.all(promises))
  .then(results => results.map(result => result.json()))
  .then(promises => Promise.all(promises))
  .then(results => results.map(starship => filterStarship(starship)))
  .then(starships => res.status(200).send({ status: 1, starships }))
  .catch(err => {
    console.log(err);
    res.status(500).send({ status: -1 });
  });
});

module.exports = router;
