const { Router } = require('express');
const fetch = require('node-fetch');

const { filterStarship } = require('../helpers/filters');
const { validateStarship } = require('../helpers/validators');
const Starship = require('../models/Starship');

const router = new Router();

router.get('/', (req, res) => {
  fetch('https://swapi.co/api/starships/?format=json')
  .then(data => data.json())
  .then(data => data.results.map(starship => filterStarship(starship)))
  .then(starships => res.status(200).send({ status: 1, starships }))
  .catch(err => {
    console.log(err);
    res.status(500).send({ status: -1 });
  });
});

router.get('/:id', (req, res) => {
  const id = req.params.id;

  Starship.findOne({ id }, (err, starship) => {
    if(err) {
      res.status(500).send({ status: -1 });
    } else {
      if(starship) {
        const { name, model, manufacturer, length } = starship;
        res.status(200).send({ status: 1, starship: { name, model, manufacturer, length } });
      } else {
        fetch(`https://swapi.co/api/starships/${id}/?format=json`)
        .then(data => data.json())
        .then(starship => filterStarship(starship))
        .then(starship => {
          if(starship) {
            res.status(200).send({ status: 1, starship });
          } else {
            res.status(404).send({ status: -2 });
          }
        })
        .catch(err => {
          console.log(err);
          res.status(500).send({ status: -1 });
        });
      }
    }
  });
});

router.put('/:id', (req, res) => {
  const id = req.params.id;
  const { name, model, manufacturer, length } = req.body;

  const validation = validateStarship({ name, model, manufacturer, length });

  if(validation.error) {
    res.status(400).send({ status: -3 });
  } else {
    Starship.updateOne({ id }, { name, model, manufacturer, length }, {upsert: true, setDefaultsOnInsert: true}, (err, row) => {
      if (err) {
        console.log(err);
        res.status(500).send({ status: -1 });
      } else {
        res.status(200).send({ status: 1 });
      }
    });
  }
});

module.exports = router;
