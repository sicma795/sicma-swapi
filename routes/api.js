const { Router } = require('express');

const router = new Router();

const films = require('./films');
const starships = require('./starships');

router.use('/films', films);
router.use('/starships', starships);

module.exports = router;
