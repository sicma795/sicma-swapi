const path = require('path');

module.exports = {
  entry: './source/client',
  output: {
    path: path.resolve(__dirname, 'public/js'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      { test: /\.js$/, use: { loader: 'babel-loader' } }
    ]
  },
  watch: true
}
