const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  id: 'Number',
  name: 'String',
  model: 'String',
  manufacturer: 'String',
  length: 'Number'
});

const Starship = mongoose.model('Starship', schema);

module.exports = Starship;
