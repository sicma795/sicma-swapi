import React, { Component } from 'react';
import { Row } from 'react-materialize';

import Heading from '../Heading';
import SideBar from '../SideBar';
import Loading from '../Loading';
import StarshipForm from './StarshipForm';

import { validateStarship } from '../../../helpers/validators';

class Starship extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchStarship = this.fetchStarship.bind(this);
    this.errors = {
      name: false,
      mode: false,
      manufacturer: false,
      length: false
    }
    this.state = {
      fetched: false,
      errors: false,
      submitDisabled: false,
      name: "",
      mode: "",
      manufacturer: "",
      length: "",
      code: 0
    }
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit() {
    const { name, model, manufacturer, length } = this.state;

    const validation = validateStarship({ name, model, manufacturer, length });
    this.errors = validation.errors;

    if(validation.error) {
      this.setState({ errors: true });
    } else {
      this.setState({ errors: false, code: 0, submitDisabled: true }, () => {
        fetch(`/api/starships/${this.props.match.params.id}`,{
          method: 'PUT',
          body: JSON.stringify({ name, model, manufacturer, length }),
          headers:{
            'Content-Type': 'application/json'
          }
        })
        .then(response => response.json())
        .then(data => this.setState({ code: data.status, submitDisabled: false }))
        .catch(err => {
          this.setState({ code: -3, submitDisabled: false });
          console.log(err)
        });
      });
    }
  }

  fetchStarship() {
    fetch(`/api/starships/${this.props.match.params.id}`)
    .then(data => data.json())
    .then(data => {
      switch(data.status) {
        case 1:
          const { name, model, manufacturer, length } = data.starship;
          this.setState({ name, model, manufacturer, length, fetched: true });
          break;
        case -2: break;
        default: this.setState({ code: -99 }); break;
      }
    })
    .catch(err => {
      alert('Fetching error');
      console.log(err)
    });
  }

  componentDidMount() {
    this.fetchStarship();
  }

  render() {
    return (
      <div>
        <Heading title={`Starship ${this.props.match.params.id}`} />
        <Row>
          <SideBar m={3} />
          {this.state.fetched ? <StarshipForm s={12} m={9} {...this.state} code={this.state.code} errors={this.errors} disableSubmit={this.state.submitDisabled} onChange={this.handleChange} onSubmit={this.handleSubmit} /> : <Loading s={12} m={9} />}
        </Row>
      </div>
    );
  }
}

export default Starship;
