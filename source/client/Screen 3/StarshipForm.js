import React from 'react';
import { Col, Input, Button, Icon } from 'react-materialize';

import { container } from '../Styles';

const styles = {
  buttonContainer: {
    textAlign: 'center',
    margin: '1em'
  },
  message: {
    textAlign: 'center'
  },
  success: {
    color: '#0cff0c',
    fontWeight: 'bold',
  },
  error: {
    color: '#ff0c0c',
    fontWeight: 'bold',
  }
}

const StarshipForm = props => (
  <Col s={props.s} m={props.m} l={props.l} xl={props.xl}>
    <Col s={12} style={container}>
      <h5>Starship: {props.name}</h5>
      <Input s={12} label="Name" placeholder="Name" name="name" validate error={props.errors.name ? 'The name cannot be empty' : null} value={props.name} onChange={e => props.onChange(e)} />
      <Input s={12} label="Model" placeholder="Model" name="model" validate error={props.errors.model ? 'The model cannot be empty' : null} value={props.model} onChange={e => props.onChange(e)} />
      <Input s={12} label="Manufacturer" placeholder="Manufacturer" validate error={props.errors.manufacturer ? 'The manufacturer cannot be empty' : null} name="manufacturer" value={props.manufacturer}  onChange={e => props.onChange(e)} />
      <Input s={12} label="Length" placeholder="Length" name="length" validate error={props.errors.length ? 'The length must be a valid number' : null} value={props.length} onChange={e => props.onChange(e)} />
      <Col s={12} style={styles.buttonContainer}>
        <Button waves="light" className="pink accent-3" disabled={props.disableSubmit} onClick={props.onSubmit}>Update<Icon left>cloud_upload</Icon></Button>
      </Col>
      <Col s={12} style={styles.message}>
        {props.code == 1 ? <p style={styles.success}>The spaceship was updated successfully</p> : null}
        {props.code == -1 ? <p style={styles.error}>Internal server error, sorry :(</p> : null}
        {props.code == -2 ? <p style={styles.error}>Bad request</p> : null}
        {props.code == -3 ? <p style={styles.error}>Network error</p> : null}
      </Col>
    </Col>
  </Col>
);

export default StarshipForm;
