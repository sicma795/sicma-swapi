import React from 'react';
import { Link } from 'react-router-dom';
import { Col, Collection, CollectionItem } from 'react-materialize';

import { container } from '../Styles';

const styles = {
  item: {
    backgroundColor: 'rgba(96, 125, 139, 0.5)',
    borderStyle: 'solid',
    borderWidth: 'thin',
    borderColor: 'white'
  }
};

const generateItem = (item, i) => {
  return (
    <Link to={`/starships/${item.id}`} key={i}>
      <CollectionItem style={styles.item}>{item.name}</CollectionItem>
    </Link>
  );
}

const StarshipsCollection = props => (
  <Col s={props.s} m={props.m} l={props.l} xl={props.xl}>
    <Col s={12} style={container}>
      <Collection>
        {props.items.map((item, i) => generateItem(item, i))}
      </Collection>
    </Col>
  </Col>
);

export default StarshipsCollection;
