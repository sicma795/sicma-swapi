import React, { Component } from 'react';
import { Row } from 'react-materialize';

import Heading from '../Heading';
import SideBar from '../SideBar';
import Loading from '../Loading';
import StarshipsCollection from './StarshipsCollection';

class Film extends Component {
  constructor(props) {
    super(props);
    this.fetchStarships = this.fetchStarships.bind(this);
    this.starships = [];
    this.state = {
      fetched: false,
      errorCode: 0
    }
  }

  fetchStarships() {
    fetch(`/api/films/${this.props.match.params.id}/starships`)
    .then(data => data.json())
    .then(data => {
      switch(data.status) {
        case 1: this.starships = data.starships; this.setState({ fetched: true }); break;
        default: this.setState({ errorCode: data.status }); break;
      }
    })
    .catch(err => console.log(err));
  }

  componentDidMount() {
    this.fetchStarships();
  }

  componentWillReceiveProps() {
    this.setState({ fetched: false }, this.fetchStarships);
  }

  render() {
    return (
      <div>
        <Heading title={`Film ${this.props.match.params.id}`} />
        <Row>
          <SideBar m={3} />
          {this.state.fetched ? <StarshipsCollection s={12} m={9} items={this.starships} /> : <Loading s={12} m={9} />}
        </Row>
      </div>
    );
  }
}

export default Film;
