import React from 'react';
import { Row, Col } from 'react-materialize';

import Heading from './Heading';
import SideBar from './SideBar';

import { container } from './Styles';

const Error = props => (
  <div>
    <Heading title="Not found" />
    <Row>
      <SideBar m={3} />
      <Col s={12} m={9}>
        <Col s={12} style={container}>
          <h4>Not found :(</h4>
        </Col>
      </Col>
    </Row>
  </div>
);

export default Error;
