import React from 'react';
import { Col } from 'react-materialize';

import { container } from './Styles';

const Loading = props => (
  <Col s={props.s} m={props.m} l={props.l} xl={props.xl}>
    <Col s={12} style={container}>
      <h3>Loading...</h3>
    </Col>
  </Col>
);

export default Loading;
