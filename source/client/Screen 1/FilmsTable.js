import React from 'react';
import { Link } from 'react-router-dom';
import {Col, Table, Button } from 'react-materialize';

import { container } from '../Styles';

const HEADINGS = ['Episode', 'Title', 'Director', 'Producer'];

const generateRows = (element, headings) => {
  return(
    <tr key={element.id}>
      {headings.map(heading => <td>{element[heading.toLowerCase()]}</td>)}
      <td>
        <Link to={`/films/${element.id}`}>
          <Button waves="light" className="pink accent-3">Go</Button>
        </Link>
      </td>
    </tr>
  );
}

const FilmsTable = props => (
  <Col s={props.s} m={props.m} l={props.l} xl={props.xl}>
    <Col s={12} style={container}>
      <Table responsive={true}>
        <thead>
          {HEADINGS.map((heading, i) => <th key={i}>{heading}</th>)}
          <th>Go</th>
        </thead>
        <tbody>
          {props.elements.map(element => generateRows(element, HEADINGS))}
        </tbody>
      </Table>
    </Col>
  </Col>
);

export default FilmsTable;
