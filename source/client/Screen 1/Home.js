import React, { Component } from 'react';
import { Row } from 'react-materialize';

import Heading from '../Heading';
import SideBar from '../SideBar';
import Loading from '../Loading';
import FilmsTable from './FilmsTable';

class Home extends Component {
  constructor(props) {
    super(props);
    this.fetchFilms = this.fetchFilms.bind(this);
    this.films = [];
    this.state = {
      fetched: false,
      errorCode: 0
    }
  }

  fetchFilms() {
    fetch('/api/films')
    .then(data => data.json())
    .then(data => {
      switch(data.status) {
        case 1: this.films = data.films; this.setState({ fetched: true }); break;
        default: this.setState({ errorCode: data.status }); break;
      }
    })
    .catch(err => console.log(err));
  }

  componentDidMount() {
    this.fetchFilms();
  }

  render() {
    return (
      <div>
        <Heading title="Films" />
        <Row>
          <SideBar m={3} />
          {this.state.fetched ? <FilmsTable s={12} m={9} elements={this.films} /> : <Loading s={12} m={9} />}
        </Row>
      </div>
    );
  }
}

export default Home;
