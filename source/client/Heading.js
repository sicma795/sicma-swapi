import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col } from 'react-materialize';

import { container } from './Styles';

const styles = {
  title: {
    textAlign: 'center'
  },
  rowContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    maxWidth: '6em',
    height: 'auto'
  }
}

const Heading = props => (
  <Row>
    <Col s={12} style={styles.rowContainer}>
      <Col s={3}><img src="/img/death-star.png" style={styles.image} /></Col>
      <Col s={9} style={styles.title}><h3>SWAPI Starships</h3></Col>
    </Col>
    <Col s={12}>
      <Row style={container}>
        <Col s={3}><h5><Link to="/">Home</Link></h5></Col>
        <Col s={9}><h5>{props.title}</h5></Col>
      </Row>
    </Col>
  </Row>
);

export default Heading;
