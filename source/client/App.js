import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Home from './Screen 1/Home';
import Film from './Screen 2/Film';
import Starship from './Screen 3/Starship';
import Error from './Error';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <BrowserRouter>
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/films/:id' exact component={Film} />
          <Route path='/starships/:id' exact component={Starship} />
          <Route path="/error" component={Error} />
          <Redirect to="/error" />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
