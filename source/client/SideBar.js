import React from 'react';
import { Col } from 'react-materialize';
import { Link } from 'react-router-dom';

import { container } from './Styles'

const styles = {
  header: {
    fontWeight: 'bold'
  }
}

const SideBar = props => (
  <Col s={props.s} m={props.m} l={props.l} xl={props.xl} className="hide-on-small-only">
    <Col s={12} style={container}>
      <p style={styles.header}>Prequel Trilogy</p>
      <ul>
        <li><Link to='/films/4'>Episode 1</Link></li>
        <li><Link to='/films/5'>Episode 2</Link></li>
        <li><Link to='/films/6'>Episode 3</Link></li>
      </ul>
      <p style={styles.header}>Original Trilogy</p>
      <ul>
        <li><Link to='/films/1'>Episode 4</Link></li>
        <li><Link to='/films/2'>Episode 5</Link></li>
        <li><Link to='/films/3'>Episode 6</Link></li>
      </ul>
      <p style={styles.header}>Sequel Trilogy</p>
      <ul>
        <li><Link to='/films/7'>Episode 7</Link></li>
      </ul>
    </Col>
  </Col>
);

export default SideBar;
