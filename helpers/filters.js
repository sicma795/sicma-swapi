const filterFilm = film => {
  const id = film.url.split('/')[5];
  const episode = film.episode_id;
  const { title, director, producer } = film;
  return { id, title, episode, director, producer  }
}

const filterStarship = starship => {
  const id = starship.url.split('/')[5];
  const { name, model, manufacturer, length  } = starship;
  return { id, name, model, manufacturer, length  }
}

module.exports = { filterFilm, filterStarship };
