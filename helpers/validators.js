const validateString = str => str.length < 1;
const validateNumber = n => !n.toString().match(/^(?:\d|\d+\.?\d+)$/g);

const validateStarship = starship => {
  const errors = {
    name: validateString(starship.name),
    model: validateString(starship.model),
    manufacturer: validateString(starship.manufacturer),
    length: validateNumber(starship.length)
  }

  const error = errors.name || errors.model || errors.manufacturer || errors.length;
  return { error, errors }
}

module.exports = { validateString, validateNumber, validateStarship }
