const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const helmet = require('helmet');
const path = require('path');

// Database config
const DB_URL = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PWD}@fantasy-iqnze.mongodb.net/swapi?retryWrites=true`;
mongoose.connect(DB_URL, { useNewUrlParser: true });
const db = mongoose.connection;

// Routes
const api = require('./routes/api');

const app = express();

app.set('port', process.env.PORT || 3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet());
app.use(bodyParser.json());

// Routing
app.use('/api', api);

app.get('/*', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, 'views', 'index.html'));
});

// Initialization sequence logic
db.once('open', () => {
  app.listen(app.get('port'), () => {
    console.log(`Listening on port ${app.get('port')}`);
  });
});
